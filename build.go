// +build ignore

/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 * Copyright (C) 2014 The Syncthing Authors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// This build script is based on "https://raw.githubusercontent.com/syncthing/syncthing/master/build.go" (2017-02-21)
// which is licensed under Mozilla Public License, v. 2.0. You can obtain the license at https://mozilla.org/MPL/2.0/.

package main

import (
	"archive/tar"
	"archive/zip"
	"bytes"
	"compress/gzip"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

var (
	version      = "0.2"
	goarch       string
	goos         string
	race         bool
	defaultDists bool
	buildDir     string
	distDir      string
	debug        = os.Getenv("BUILDDEBUG") != ""

	defaultOsAndArchs = []struct{ os, arch string }{
		{"windows", "386"},
		{"windows", "amd64"},
		{"linux", "386"},
		{"linux", "amd64"},
		{"linux", "arm"},
		{"linux", "arm64"},
		{"darwin", "386"},
		{"darwin", "amd64"},
	}
)

func init() {
	cwd, _ := os.Getwd()
	buildDir = filepath.Join(cwd, "/build")
	distDir = filepath.Join(cwd, "/dist")
}

type target struct {
	name         string
	buildPkg     string
	binaryName   string
	archiveFiles []archiveFile
	tags         []string
}

func (t target) BinaryName() string {
	binary := t.binaryName
	if goos == "windows" {
		binary = binary + ".exe"
	}
	return binary
}

type archiveFile struct {
	src  string
	dst  string
	perm os.FileMode
}

func parseFlags() {
	flag.StringVar(&goarch, "goarch", runtime.GOARCH, "GOARCH")
	flag.StringVar(&goos, "goos", runtime.GOOS, "GOOS")
	flag.BoolVar(&defaultDists, "default-dists", false, fmt.Sprint("Create default distributions: ", defaultOsAndArchs))
	flag.Parse()
}

func main() {
	log.SetOutput(os.Stdout)
	log.SetFlags(0)

	os.Setenv("GO15VENDOREXPERIMENT", "1")
	parseFlags()

	t := &target{
		name:       "rhttp-gateway",
		binaryName: "rhttp-gateway",
		buildPkg:   "./rhttp-gateway",
		archiveFiles: []archiveFile{
			{src: "{{binary}}", dst: "{{binary}}", perm: 0755},
			{src: "rhttp-gateway/config/rhttp-gateway.xml", dst: "config/rhttp-gateway.xml", perm: 0644},
			{src: "README.md", dst: "README.md", perm: 0644},
			{src: filepath.Join(buildDir, "LICENSES"), dst: "LICENSE", perm: 0644},
		},
	}

	if defaultDists {
		for _, osarch := range defaultOsAndArchs {
			goos = osarch.os
			goarch = osarch.arch
			runCommand("zip", *t)
			runCommand("tar", *t)
		}
	} else {
		if flag.NArg() == 0 {
			runCommand("build", *t)
		} else {
			runCommand(flag.Arg(0), *t)
		}
	}
}

func setup() {
	packages := []string{
		"github.com/kardianos/govendor",
	}
	for _, pkg := range packages {
		fmt.Println(pkg)
		runPrint("go", "get", "-u", pkg)
	}
}

func runCommand(cmd string, target target) {
	switch cmd {
	case "setup":
		setup()
	case "build":
		build(target, nil)
	case "zip":
		buildArchive(target, "zip")
	case "tar", "tar.gz", "tgz":
		buildArchive(target, "tar.gz")
	case "clean":
		rmr(distDir, buildDir)
	default:
		log.Fatalf("Unknown command %q", cmd)
	}
}

func mkdirs(paths ...string) {
	for _, path := range paths {
		if debug {
			log.Println("mkdir", path)
		}
		os.MkdirAll(path, os.ModePerm)
	}
}

func rmr(paths ...string) {
	for _, path := range paths {
		if debug {
			log.Println("rm -r", path)
		}
		os.RemoveAll(path)
	}
}

func build(target target, tags []string) {

	tags = append(target.tags, tags...)

	binary := filepath.Join(buildDir, target.BinaryName())

	args := []string{"build", "-v", "-o", binary}
	if len(tags) > 0 {
		args = append(args, "-tags", strings.Join(tags, " "))
	}

	args = append(args, target.buildPkg)

	os.Setenv("GOOS", goos)
	os.Setenv("GOARCH", goarch)
	runPrint("go", args...)
}

func createLicense(target target) {
	args := []string{"license", "-o", filepath.Join(buildDir, "LICENSES")}

	os.Setenv("GOOS", goos)
	os.Setenv("GOARCH", goarch)
	runPrint("govendor", args...)
}

func runPrint(cmd string, args ...string) {
	if debug {
		t0 := time.Now()
		log.Println("runPrint:", cmd, strings.Join(args, " "))
		defer func() {
			log.Println("... in", time.Since(t0))
		}()
	}
	ecmd := exec.Command(cmd, args...)
	ecmd.Stdout = os.Stdout
	ecmd.Stderr = os.Stderr
	err := ecmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func buildArchive(target target, archiveFormat string) {

	name := archiveName(target)
	filename := filepath.Join(distDir, name+"."+archiveFormat)

	build(target, nil)
	createLicense(target)

	archiveFiles := make([]archiveFile, len(target.archiveFiles))

	for i := range target.archiveFiles {
		archiveFiles[i].src = strings.Replace(target.archiveFiles[i].src, "{{binary}}", filepath.Join(buildDir, target.BinaryName()), 1)
		archiveFiles[i].dst = strings.Replace(target.archiveFiles[i].dst, "{{binary}}", target.BinaryName(), 1)
		archiveFiles[i].dst = name + "/" + archiveFiles[i].dst
	}

	mkdirs(filepath.Dir(filename))

	switch archiveFormat {
	case "zip":
		zipFile(filename, archiveFiles)
	case "tar.gz":
		tarGz(filename, archiveFiles)
	default:
		log.Fatalf("Unknown archive format %q", archiveFormat)
	}

	log.Println(filename)
}

func zipFile(out string, files []archiveFile) {
	fd, err := os.Create(out)
	if err != nil {
		log.Fatal(err)
	}

	zw := zip.NewWriter(fd)

	for _, f := range files {
		sf, err := os.Open(f.src)
		if err != nil {
			log.Fatal(err)
		}

		info, err := sf.Stat()
		if err != nil {
			log.Fatal(err)
		}

		fh, err := zip.FileInfoHeader(info)
		if err != nil {
			log.Fatal(err)
		}
		fh.Name = filepath.ToSlash(f.dst)
		fh.Method = zip.Deflate

		if strings.HasSuffix(f.dst, ".txt") {
			// Text file. Read it and convert line endings.
			bs, err := ioutil.ReadAll(sf)
			if err != nil {
				log.Fatal(err)
			}
			bs = bytes.Replace(bs, []byte{'\n'}, []byte{'\n', '\r'}, -1)
			fh.UncompressedSize = uint32(len(bs))
			fh.UncompressedSize64 = uint64(len(bs))

			of, err := zw.CreateHeader(fh)
			if err != nil {
				log.Fatal(err)
			}
			of.Write(bs)
		} else {
			// Binary file. Copy verbatim.
			of, err := zw.CreateHeader(fh)
			if err != nil {
				log.Fatal(err)
			}
			_, err = io.Copy(of, sf)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	err = zw.Close()
	if err != nil {
		log.Fatal(err)
	}
	err = fd.Close()
	if err != nil {
		log.Fatal(err)
	}
}

func tarGz(out string, files []archiveFile) {
	fd, err := os.Create(out)
	if err != nil {
		log.Fatal(err)
	}

	gw := gzip.NewWriter(fd)
	tw := tar.NewWriter(gw)

	for _, f := range files {
		sf, err := os.Open(f.src)
		if err != nil {
			log.Fatal(err)
		}

		info, err := sf.Stat()
		if err != nil {
			log.Fatal(err)
		}
		h := &tar.Header{
			Name:    f.dst,
			Size:    info.Size(),
			Mode:    int64(info.Mode()),
			ModTime: info.ModTime(),
		}

		err = tw.WriteHeader(h)
		if err != nil {
			log.Fatal(err)
		}
		_, err = io.Copy(tw, sf)
		if err != nil {
			log.Fatal(err)
		}
		sf.Close()
	}

	err = tw.Close()
	if err != nil {
		log.Fatal(err)
	}
	err = gw.Close()
	if err != nil {
		log.Fatal(err)
	}
	err = fd.Close()
	if err != nil {
		log.Fatal(err)
	}
}

func archiveName(target target) string {
	return fmt.Sprintf("%s-%s-%s", target.name, buildArch(), version)
}

func buildArch() string {
	os := goos
	if os == "darwin" {
		os = "macosx"
	}
	return fmt.Sprintf("%s-%s", os, goarch)
}
