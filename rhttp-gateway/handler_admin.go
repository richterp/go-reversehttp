/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"time"

	"github.com/gin-gonic/gin"
)

func AdminBasicAuth(r *rhttpGateway) gin.HandlerFunc {
	return BasicAuth(
		func(c *gin.Context) bool {
			return true
		},
		func(c *gin.Context, user string, password string) bool {
			return user == "admin" &&
				nil == CompareHashAndPassword(r.config.AdminPassword, password)
		},
		func(c *gin.Context) string {
			return "$$$admin$$$"
		})
}

func addAdminHandlers(r *rhttpGateway, routes gin.IRoutes) {
	routes.GET("/status", showStatusHandler(r))
	routes.GET("/status/:app", findAppHandler(r), showAppStatusHandler(r))
	routes.GET("/config", showConfigHandler(r))
	routes.GET("/config/:app", findAppHandler(r), showAppConfig(r))
}

func showConfigHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.XML(200, r.config)
	}
}

func showAppConfig(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		app := c.MustGet("app").(*app)
		c.JSON(200, app.Config)
	}
}

func showStatusHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		apps := gin.H{}
		r.appRegistry.registryM.RLock()
		for appId, app := range r.appRegistry.registry {
			apps[appId] = appStatus(app, r)
		}
		r.appRegistry.registryM.RUnlock()
		c.JSON(200, gin.H{"date": time.Now(), "apps": apps})
	}
}

func showAppStatusHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		app := c.MustGet("app").(*app)
		japp := appStatus(app, r)
		c.JSON(200, gin.H{"date": time.Now(), "app": japp})
	}
}

func appStatus(app *app, r *rhttpGateway) gin.H {
	return gin.H{
		"ActivePolls":      len(app.activePolls),
		"PendingResponses": len(app.responses),
		"MaxParallelPolls": app.Config.MaxParallelPolls,
		"PublicURL":        app.PublicURL,
		"Lease":            app.Config.Lease,
		"Config":           r.config.GatewayBaseURL + "/admin/config/" + app.Config.Name,
	}
}
