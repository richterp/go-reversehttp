/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"encoding/xml"
	"io"
	"os"
	"sync"
	"time"
)

type ServerConfig struct {
	XMLName            xml.Name        `xml:"gateway"`
	AdminPassword      string          `xml:"adminPassword"`
	ClientBinding      string          `xml:"clientBinding"`
	ClientTls          []TlsCertConfig `xml:"clientTls>cert"`
	FrontendBinding    string          `xml:"frontendBinding"`
	FrontendTls        []TlsCertConfig `xml:"frontendTls>cert"`
	GatewayBaseURL     string          `xml:"clientURL"`
	PublicTemplateURL  string          `xml:"frontendURL"`
	MaxParallelPolls   uint            `xml:"maxParallelPolls"`
	AllowPublicAccess  bool            `xml:"openGateway"`
	PollTimeout        *Timeout        `xml:"pollTimeout"`
	AppRequestTimeout  *Timeout        `xml:"appRequestTimeout"`
	AppResponseTimeout *Timeout        `xml:"appResponseTimeout"`
	Apps               []AppConfig     `xml:"apps>app"`
	Users              []UserConfig    `xml:"users>user"`
	WildcardApps       WildcardApps    `xml:"wildcardApps"`
}

func NewServerConfig() *ServerConfig {
	serverConfig := &ServerConfig{}
	serverConfig.ClientBinding = ":8000"
	serverConfig.FrontendBinding = ":8001"
	serverConfig.GatewayBaseURL = "http://localhost:8000/reversehttp"
	serverConfig.PublicTemplateURL = "http://APP-NAME.localhost:8001"
	serverConfig.AppRequestTimeout = &Timeout{Duration: "15s"}
	serverConfig.AppResponseTimeout = &Timeout{Duration: "10m"}
	serverConfig.PollTimeout = &Timeout{Duration: "5m"}
	serverConfig.AdminPassword = ""
	serverConfig.MaxParallelPolls = 256

	return serverConfig
}

type TlsCertConfig struct {
	KeyFile  string `xml:"keyFile,attr"`
	CertFile string `xml:"certFile,attr"`
}

type Timeout struct {
	Duration string `xml:"value,attr"`
	dur      *time.Duration
	durM     sync.Mutex
}

func (t *Timeout) toDuration() time.Duration {
	if t.dur == nil {
		t.durM.Lock()
		if t.dur == nil {
			dur, _ := time.ParseDuration(t.Duration)
			t.dur = &dur
		}
		t.durM.Unlock()
	}
	return *t.dur
}

type AppConfig struct {
	Name             string `xml:"name,attr"`
	Token            string `xml:"token,attr"`
	MaxParallelPolls uint   `xml:"maxParallelPolls,attr"`
	Lease            int
	FrontendUsers    []UserConfig `xml:"frontendAuth>user"`
	//Clients          []UserConfig `xml:"clients>user"`
}

type UserConfig struct {
	Name     string `xml:"name,attr"`
	Password string `xml:"password,attr"`
	Ref      string `xml:"ref,attr"`
}

func LoadConfigFile(f string, c *ServerConfig) error {
	reader, err := os.Open(f)
	defer reader.Close()
	if err != nil {
		return err
	}
	return LoadConfig(reader, c)
}

func LoadConfig(r io.Reader, c *ServerConfig) error {
	decoder := xml.NewDecoder(r)
	err := decoder.Decode(c)

	normalize(c)

	if err != nil {
		return err
	}
	return nil
}

func normalize(sc *ServerConfig) {

	if sc.MaxParallelPolls == 0 {
		sc.MaxParallelPolls = 99
	}

	for i, _ := range sc.Apps {
		maxParallelPolls := sc.Apps[i].MaxParallelPolls
		if maxParallelPolls == 0 {
			maxParallelPolls = sc.MaxParallelPolls
		}
		sc.Apps[i].MaxParallelPolls = maxParallelPolls
	}
}

type WildcardApps struct {
	Fmt   string `xml:"fmt,attr"`
	Count int    `xml:"count,attr"`
}
