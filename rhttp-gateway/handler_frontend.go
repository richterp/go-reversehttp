/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"net/http"
	"net/url"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
)

func AppToContextByHostname(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		logEntry := CreateHandlerLog("AppToContextByHostname", c, log.Fields{
			"host": c.Request.Host,
		})

		appId, ok := getAppName(c.Request, r.config.PublicTemplateURL)
		logEntry = logEntry.WithField("appId", appId)
		if !ok {
			c.AbortWithStatus(http.StatusNotFound)
			logEntry.Debug("App name could not be retrieved from request!")
			return
		}
		logEntry.Debug("App name retrieved from request.")

		app, ok := r.appRegistry.findApp(appId)
		if !ok {
			c.AbortWithStatus(http.StatusNotFound)
			logEntry.Debug("App not found!")
			return
		}
		logEntry.Debug("App found.")

		c.Set("app", app)
		c.Next()
	}
}

func frontendHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {

		logEntry := CreateHandlerLog("frontendHandler", c, log.Fields{
			"host": c.Request.Host,
			"path": c.Request.URL.Path,
		})

		app := c.MustGet("app").(*app)

		if ok := updatePath(c.Request, r.config.PublicTemplateURL, app.Config.Name); !ok {
			c.Status(http.StatusNotFound)
			return
		}

		ri := &requestItem{c.Request, make(chan *http.Request), make(chan struct{}), ""}

		defer func() {
			app.consumeResponse(ri.requestId)
			close(ri.done)
		}()

		stopId, stopNotify := r.stopNotifier.StopNotify()
		defer r.stopNotifier.Delete(stopId)

		logEntry.Debug("Waiting for app client to pass request.")
		select {
		case <-c.Writer.CloseNotify():
			logEntry.Debug("Frontend client close.")
			return
		case <-time.After(r.config.AppRequestTimeout.toDuration()):
			logEntry.Error("AppRequestTimeout!")
			c.Status(http.StatusGatewayTimeout)
			return
		case <-stopNotify:
			logEntry.Debug("Rhttp gateway will stop!")
			c.Status(http.StatusServiceUnavailable)
			return
		case app.requestItems <- ri:
			// someone took my requestItem.
		}

		app.leaseTimer.StopLeaseTimer()
		defer app.leaseTimer.ResetLeaseTimer()

		var response *http.Request

		select {
		case <-c.Writer.CloseNotify():
			logEntry.Debug("Frontend client close.")
			return
		case <-time.After(r.config.AppResponseTimeout.toDuration()):
			logEntry.Error("AppResponseTimeout!")
			c.Request.Body.Close()
			c.Status(http.StatusGatewayTimeout)
			return
		case <-stopNotify:
			logEntry.Debug("Rhttp gateway will stop!")
			c.Request.Body.Close()
			c.Status(http.StatusServiceUnavailable)
			return
		case response = <-ri.response:

			logEntry.Debug("Passing response from app client to frontend client.")
			c.Request.Body.Close()

			if err := writeResponseEntityToResponse(response, c.Writer); err != nil {
				logEntry.WithError(err).Error("Error writing response entity!")
				return
			}
		}
	}
}

func getAppName(request *http.Request, publicTemplateURL string) (string, bool) {
	_publicTemplateURL, _ := url.Parse(publicTemplateURL)

	if i := strings.Index(_publicTemplateURL.Host, APP_VAR); i >= 0 {
		name := request.Host[i:]
		if i := strings.Index(name, "."); i >= 0 {
			name = name[:i]
		}
		return name, true
	} else if i := strings.Index(_publicTemplateURL.Path, APP_VAR); i >= 0 {
		name := request.URL.Path[i:]
		if i := strings.Index(name, "/"); i >= 0 {
			name = name[:i]
		}
		return name, true
	}
	return "", false

}

func updatePath(request *http.Request, publicTemplateURL, appName string) bool {

	actualPath := request.URL.Path
	i := 0

	if i = strings.Index(publicTemplateURL, "://"); i < 0 {
		return false
	}

	templPath := publicTemplateURL[i+3:]

	if i = strings.Index(templPath, "/"); i < 0 {
		return true
	}

	templPath = templPath[i:]
	templPath = strings.Replace(templPath, APP_VAR, appName, -1)
	if len(templPath) > len(actualPath) {
		return false
	}
	actualPath = actualPath[len(templPath):]
	if strings.Index(actualPath, "/") != 0 {
		actualPath = "/" + actualPath
	}
	request.URL.Path = actualPath

	return true
}
