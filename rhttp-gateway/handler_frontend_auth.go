/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
)

func FrontendBasicAuth() gin.HandlerFunc {
	return BasicAuth(
		shouldAuth,
		bcryptAuth,
		authName)
}

func shouldAuth(c *gin.Context) bool {
	app := c.MustGet("app").(*app)
	return len(app.FrontendUsers) > 0
}

func authName(c *gin.Context) string {
	app := c.MustGet("app").(*app)
	return app.Config.Name
}

func bcryptAuth(c *gin.Context, user string, password string) bool {
	app := c.MustGet("app").(*app)
	log.WithFields(log.Fields{"user": user, "app": app.Config.Name}).Debug("Authenticating using CompareHashAndPassword.")
	u := app.FrontendUsers[user]
	return u != nil &&
		nil == CompareHashAndPassword(u.Password, password)
}
