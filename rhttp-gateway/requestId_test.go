/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"regexp"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRequestIdUnique(t *testing.T) {
	const tries = 65536
	var wg sync.WaitGroup
	var mu sync.Mutex
	wg.Add(tries)
	m := make(map[string]struct{})
	for i := 0; i < tries; i++ {
		go func() {
			defer wg.Done()
			id := newId()
			mu.Lock()
			m[id] = struct{}{}
			mu.Unlock()
		}()
	}
	wg.Wait()
	assert.Equal(t, tries, len(m), "Request IDs are not unique.")
}

func TestRequestIdFormat(t *testing.T) {
	const tries = 65536
	re := regexp.MustCompile("^[A-F0-9]{16}$")
	for i := 0; i < tries; i++ {
		if passed := assert.True(t, re.MatchString(newId()), "Request ID has wrong format."); !passed {
			break
		}
	}
}
