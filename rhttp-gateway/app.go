/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"sync"
)

const (
	MIN_LEASE      = 10
	MAX_LEASE      = 24 * 60 * 60
	INFINITE_LEASE = -1
)

type requestItem struct {
	request   *http.Request
	response  chan *http.Request
	done      chan struct{}
	requestId string
}

type app struct {
	activePolls   chan struct{}
	requestItems  chan *requestItem
	responses     map[string]*requestItem
	responsesM    sync.RWMutex
	validPollIds  map[string]struct{}
	validPollIdsM sync.RWMutex
	PublicURL     string
	Config        *AppConfig
	FrontendUsers map[string]*UserConfig
	leaseTimer    *LeaseTimer
}

func appNameValid(name string) error {
	if len(name) < 1 || len(name) > 63 {
		return errors.New("length of app name not valid: " + fmt.Sprint(len(name)))
	}
	for _, c := range name {
		if !((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '-') {
			return errors.New("character in app name not valid: " + string(c))
		}
	}
	if name[0] == '-' || name[len(name)-1] == '-' {
		return errors.New("app name MUST NOT start or end with a '-' (dash)")
	}
	{
		var i int
		for _, c := range name {
			if !(c >= '0' && c <= '9') {
				break
			}
			i++
		}
		if i == len(name) {
			return errors.New("app name MUST NOT consist of all numeric values")
		}
	}
	return nil
}

func newApp(ac *AppConfig, sc *ServerConfig, userRegistry *userRegistry, persistent bool) (*app, error) {
	if err := appNameValid(ac.Name); err != nil {
		return nil, err
	}

	if persistent && ac.Lease <= 0 {
		ac.Lease = INFINITE_LEASE
	} else {
		ac.Lease = min(max(ac.Lease, MIN_LEASE), MAX_LEASE)
	}

	publicURL := strings.Replace(sc.PublicTemplateURL, APP_VAR, ac.Name, -1)

	users := userRegistry.getUsers(ac.FrontendUsers)

	app := &app{
		activePolls:   make(chan struct{}, ac.MaxParallelPolls),
		requestItems:  make(chan *requestItem),
		responses:     make(map[string]*requestItem),
		validPollIds:  make(map[string]struct{}),
		PublicURL:     publicURL,
		Config:        ac,
		FrontendUsers: users,
	}

	return app, nil
}

func (app *app) newRequestId() string {
	requestId := newId()
	app.addValidRequestId(requestId)
	return requestId
}

func (app *app) addValidRequestId(requestId string) {
	app.validPollIdsM.Lock()
	app.validPollIds[requestId] = struct{}{}
	app.validPollIdsM.Unlock()
}

func (app *app) isRequestIdValid(requestId string) bool {
	//lock necessary?
	app.validPollIdsM.Lock()
	_, ok := app.validPollIds[requestId]
	app.validPollIdsM.Unlock()
	return ok
}

func (app *app) invalidateRequestId(requestId string) bool {
	app.validPollIdsM.Lock()
	_, ok := app.validPollIds[requestId]
	delete(app.validPollIds, requestId)
	app.validPollIdsM.Unlock()
	return ok
}

func (app *app) addResponse(requestId string, ri *requestItem) {
	ri.requestId = requestId
	app.responsesM.Lock()
	app.responses[requestId] = ri
	app.responsesM.Unlock()
}
func (app *app) consumeResponse(requestId string) (*requestItem, bool) {
	app.responsesM.Lock()
	ri, ok := app.responses[requestId]
	delete(app.responses, requestId)
	app.responsesM.Unlock()
	return ri, ok
}

func uint64_min(x, y uint64) uint64 {
	if x < y {
		return x
	}
	return y
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func uint64_max(x, y uint64) uint64 {
	if x > y {
		return x
	}
	return y
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
