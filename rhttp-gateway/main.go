/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"syscall"

	"golang.org/x/crypto/ssh/terminal"

	log "github.com/Sirupsen/logrus"
	"github.com/urfave/cli"
)

const (
	opt_config          = "config"
	opt_log_output      = "log"
	opt_log_force_color = "color"
	opt_debug           = "debug"
)

const (
	APP_NAME      = "rhttp-gateway"
	APP_USAGE     = "Reverse HTTP Gateway"
	APP_VERSION   = "0.2"
	APP_COPYRIGHT = "(c) 2017 Pascal Richter <pascal.richter@kupanet.de>"
)

func main() {

	app := cli.NewApp()
	app.Name = APP_NAME
	app.Usage = APP_USAGE
	app.Version = APP_VERSION
	app.Authors = []cli.Author{
		cli.Author{
			Name:  "Pascal Richter",
			Email: "pascal.richter@kupanet.de",
		},
	}
	app.Copyright = APP_COPYRIGHT
	app.ArgsUsage = " "

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  opt_config + ", c",
			Usage: "Load configuration from `FILE`",
		},
		cli.BoolFlag{
			Name:  opt_debug,
			Usage: "Enable debug mode",
		},
		cli.StringFlag{
			Name:  opt_log_output,
			Usage: "Write log to `FILE`. If FILE is '-' log is written to STDOUT.",
		},
		cli.BoolFlag{
			Name:  opt_log_force_color,
			Usage: "Force color logs.",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:    "crypt",
			Aliases: []string{"c"},
			Usage:   "Hashes a password or token.",
			Action: func(c *cli.Context) error {

				fmt.Print("Password: ")
				var password string

				// Non echoing mode...
				passwordBytes, err := terminal.ReadPassword(int(syscall.Stdin))
				if err != nil {
					// ... and fallback (cygwin)
					scanner := bufio.NewScanner(os.Stdin)
					scanner.Scan()
					password = scanner.Text()
				} else {
					fmt.Println("")
					password = string(passwordBytes)
				}

				if hashedPassword, err := HashPassword(password); err == nil {
					fmt.Println(hashedPassword)
				} else {
					fmt.Println("Error: ", err)
				}

				return nil
			},
		},
	}

	app.Action = func(c *cli.Context) error {

		SetMode(ProdMode)
		if c.IsSet(opt_debug) {
			SetMode(DebugMode)
		}

		configureLogging(c)

		config, err := readConfig(c)
		if err != nil {
			return cli.NewExitError(err, 1)
		}

		rhttpGateway := NewRhttpGateway(config)
		rhttpGateway.Run()

		return nil
	}

	app.Run(os.Args)

}

func readConfig(c *cli.Context) (*ServerConfig, error) {
	configFile := c.String(opt_config)
	if !c.IsSet(opt_config) {
		configFile, _ = lookupConfigFile()
		if len(configFile) == 0 {
			log.WithFields(log.Fields{"configFileLocations": configFileLocations}).
				Error("No config file found at standard locations and no config file was given as command line parameter.")
			return nil, errors.New("No config file found.")
		}
	}

	log.Info("Loading config file: ", configFile)
	config := NewServerConfig()
	if err := LoadConfigFile(configFile, config); err != nil {
		log.WithError(err).Error("Error loading config file: ", configFile)
		return nil, err
	}
	return config, nil
}

func configureLogging(c *cli.Context) {
	// log formatter
	formatter := &log.TextFormatter{ForceColors: c.IsSet(opt_log_force_color)}
	log.SetFormatter(formatter)

	// log level
	if Mode() == DebugMode {
		log.SetLevel(log.DebugLevel)
	} else if Mode() == ProdMode {
		log.SetLevel(log.InfoLevel)
	}

	// log output
	if !c.IsSet(opt_log_output) || c.String(opt_log_output) == "-" {
		log.SetOutput(os.Stdout)
	} else {
		fileName := c.String(opt_log_output)
		file, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			log.SetOutput(os.Stdout)
			log.WithError(err).Error("Could not open error log file: ", fileName)
		} else {
			formatter.DisableColors = true
			log.SetOutput(file)
		}
	}
}

func lookupConfigFile() (string, error) {
	var result string
	var err error
	for _, l := range configFileLocations {
		_, err = os.Stat(l)
		if err != nil {
			continue
		}
		result, err = filepath.Abs(l)
		if err != nil {
			continue
		}
		return result, nil
	}
	return result, err
}
