/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
)

var cookieStore = sessions.NewCookieStore([]byte(securecookie.GenerateRandomKey(32)))

type rhttpSession sessions.Session

func getOrCreateRhttpSession(cookieStore *sessions.CookieStore, req *http.Request) rhttpSession {
	session, _ := cookieStore.Get(req, "rhttp-session")
	return rhttpSession(*session)
}

func (session *rhttpSession) IsAuthenticated(appName string) bool {
	auth := session.Values[appName+":auth"]
	return auth != nil && auth.(bool)
}

func (session *rhttpSession) SetAuthenticated(appName string, auth bool) {
	session.Values[appName+":auth"] = auth
}

func (session *rhttpSession) Save(r *http.Request, w http.ResponseWriter) error {
	s := sessions.Session(*session)
	return s.Save(r, w)
}

func (session *rhttpSession) Clear(request *http.Request) {
	context.Clear(request)
}
