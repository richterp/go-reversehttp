/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"github.com/goji/httpauth"
	"golang.org/x/crypto/bcrypt"
)

func CompareHashAndPassword(hashedPassword, plaintextPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(plaintextPassword))
}

func HashPassword(password string) (string, error) {
	p, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(p), err
}

type ShouldAuthFunc func(c *gin.Context) bool
type AuthFunc func(c *gin.Context, user string, password string) bool
type AuthNameFunc func(c *gin.Context) string

func BasicAuth(shouldAuthFunc ShouldAuthFunc, authFunc AuthFunc, authNameFunc AuthNameFunc) gin.HandlerFunc {
	return func(c *gin.Context) {

		session := getOrCreateRhttpSession(cookieStore, c.Request)

		// Important Note: If you aren't using gorilla/mux, you need to wrap your
		// handlers with context.ClearHandler as or else you will leak memory!
		// See https://github.com/gorilla/sessions
		defer session.Clear(c.Request)

		authName := authNameFunc(c)

		if !shouldAuthFunc(c) || session.IsAuthenticated(authName) {
			c.Next()
			return
		}

		authorized := false
		authOptions := httpauth.AuthOptions{
			Realm:               "Restricted",
			AuthFunc:            func(user string, password string, request *http.Request) bool { return authFunc(c, user, password) },
			UnauthorizedHandler: http.HandlerFunc(func(r http.ResponseWriter, e *http.Request) {}),
		}

		gin.WrapH(httpauth.BasicAuth(authOptions)(http.HandlerFunc(func(r http.ResponseWriter, e *http.Request) {
			authorized = true
		})))(c)

		session.SetAuthenticated(authName, authorized)
		if err := session.Save(c.Request, c.Writer); err != nil {
			log.WithError(err).Error("Could not save session.")
		}

		if authorized {
			log.Debug("Authenticating basicAuth sucess")
			c.Next()
		} else {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

	}
}
