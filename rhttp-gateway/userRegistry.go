/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"sync"
)

type userRegistry struct {
	registry  map[string]*UserConfig
	registryM sync.RWMutex
}

func NewUserRegistry() *userRegistry {
	return &userRegistry{registry: make(map[string]*UserConfig)}
}

func (r *userRegistry) load(sc *ServerConfig) error {
	for _, uc := range sc.Users {
		uc := uc

		r.registryM.Lock()
		r.registry[uc.Name] = &uc
		r.registryM.Unlock()
	}
	return nil
}

func (ar *userRegistry) findUser(user string) (*UserConfig, bool) {
	ar.registryM.RLock()
	u, ok := ar.registry[user]
	ar.registryM.RUnlock()
	return u, ok
}

func (userRegistry *userRegistry) getUsers(users []UserConfig) map[string]*UserConfig {
	result := make(map[string]*UserConfig)
	for _, uc := range users {
		uc := uc
		if len(uc.Ref) > 0 {
			gloUser, ok := userRegistry.findUser(uc.Ref)
			if ok {
				result[gloUser.Name] = gloUser
			}
		} else {
			result[uc.Name] = &uc
		}
	}
	return result
}
