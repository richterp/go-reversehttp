/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func findAppHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		appId := c.Param("app")

		logEntry := CreateHandlerLog("findApp", c, log.Fields{"appId": appId})

		app, ok := r.appRegistry.findApp(appId)
		if !ok {
			c.AbortWithStatus(http.StatusNotFound)
			logEntry.Warn("App not found!")
			return
		}
		logEntry.Debug("App found.")

		c.Set("app", app)
		c.Next()
	}
}

func pollHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {

		app := c.MustGet("app").(*app)
		requestId := c.Param("id")

		logEntry := CreateHandlerLog("pollHandler", c, log.Fields{"appId": app.Config.Name, "requestId": requestId})

		ok := app.invalidateRequestId(requestId)
		if !ok {
			c.Status(http.StatusNotFound)
			logEntry.Warn("Invalid request id!")
			return
		}
		logEntry.Debug("Request id is valid.")

		app.leaseTimer.StopLeaseTimer()
		defer app.leaseTimer.ResetLeaseTimer()

		select {
		case app.activePolls <- struct{}{}:
			defer func() { <-app.activePolls }()
		default:
			c.Status(http.StatusTooManyRequests)
			logEntry.Warn("Too many active polls!")
			return
		}

		stopID, stopNotify := r.stopNotifier.StopNotify()
		defer r.stopNotifier.Delete(stopID)

		logEntry.Debug("Wait till request item is available.")
		var ri *requestItem
		select {
		case <-c.Writer.CloseNotify():
			logEntry.Info("Client closes the connection.")
			return
		case <-time.After(r.config.PollTimeout.toDuration()):
			app.addValidRequestId(requestId)
			addPollRequestLinkHeader(c.Writer.Header(), r.config.GatewayBaseURL, app.Config.Name, requestId, "next")
			c.Status(204)
			logEntry.Debug("Poll timeouts.")
			return
		case <-stopNotify:
			c.Status(http.StatusServiceUnavailable)
			logEntry.Debug("Rhttp gateway will stop!")
			return
		case ri = <-app.requestItems:
			//just waiting for requestItems
		}

		logEntry.Debug("Request item received.")
		app.addResponse(requestId, ri)

		nextRequestId := app.newRequestId()
		logEntry.WithField("nextRequestId", nextRequestId).Debug("Just generated the next request id.")

		addPollRequestLinkHeader(c.Writer.Header(), r.config.GatewayBaseURL, app.Config.Name, nextRequestId, "next")

		c.Status(http.StatusOK)

		if err := writeRequestToResponse(ri.request, c.Writer); err != nil {
			logEntry.Error("Error while writing frontend request to app client response: ", err)
			return
		}
	}
}

func responseHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {

		app := c.MustGet("app").(*app)
		requestId := c.Param("id")

		logEntry := CreateHandlerLog("responseHandler", c, log.Fields{"appId": app.Config.Name, "requestId": requestId})

		ri, ok := app.consumeResponse(requestId)
		if !ok {
			c.Status(http.StatusNotFound)
			logEntry.Warn("Invalid request id!")
			return
		}
		logEntry.Debug("RequestId is now invalidated.")

		app.leaseTimer.StopLeaseTimer()
		defer app.leaseTimer.ResetLeaseTimer()

		logEntry.Debug("Waiting for frontend to deliver response.")
		select {
		case <-ri.done:
		case ri.response <- c.Request:
			<-ri.done
		}

		discard(c.Request.Body)
		c.Request.Body.Close()

		c.Status(202)
		logEntry.Debug("Handling app client response finished.")
	}
}

func registerClientHandler(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		name, _ := c.GetPostForm("name")
		token, _ := c.GetPostForm("token")
		lease, _ := c.GetPostForm("lease")

		logEntry := CreateHandlerLog("registerClientHandler", c, log.Fields{"appId": name})
		logEntry.Debug("App registration attempt!")

		app, appFoundOk := r.appRegistry.findApp(name)

		// if app not found --> find app again, now with locks
		if !appFoundOk && r.config.AllowPublicAccess {
			r.appRegistry.RegisterNewAppLock.Lock()
			app, appFoundOk = r.appRegistry.findApp(name)
			if appFoundOk {
				r.appRegistry.RegisterNewAppLock.Unlock()
			} else {
				defer r.appRegistry.RegisterNewAppLock.Unlock()
			}
		}

		if !appFoundOk {
			if r.config.AllowPublicAccess {
				//new app registration
				logEntry.Debug("App registration attempt (new)!")
				if len(token) == 0 {
					// choose random token
					token = newId()
				}

				token, err := HashPassword(token)
				if err != nil {
					logEntry.WithError(err).Error("App registration failed! Could not create hashed token.")
					c.AbortWithStatus(http.StatusInternalServerError)
					return
				}

				// wildcard app registration
				if name == "*" && r.appRegistry.wildcardAppCount > 0 {
					offset := rand.Intn(r.appRegistry.wildcardAppCount)
					for i := 0; i < r.appRegistry.wildcardAppCount; i++ {
						name = fmt.Sprintf(r.appRegistry.wildcardAppFmt, (offset+i)%r.appRegistry.wildcardAppCount)
						if _, found := r.appRegistry.findApp(name); !found {
							break
						}
						name = ""
					}
					if name == "" {
						logEntry.WithError(err).Error("Wildcard app registration failed! No free wildcard app available!")
						c.AbortWithStatus(http.StatusInternalServerError)
						return
					}
				}

				leaseInt, _ := strconv.Atoi(lease)

				if nApp, err := newApp(
					&AppConfig{
						Name:             name,
						Token:            token,
						Lease:            leaseInt,
						MaxParallelPolls: r.config.MaxParallelPolls},
					r.config,
					r.userRegistry,
					false,
				); err != nil {
					logEntry.WithError(err).Warn("App registration failed!")
					c.AbortWithStatus(http.StatusBadRequest)
					return
				} else {
					app = r.appRegistry.add(nApp)
					c.Status(201)
				}

			} else {
				logEntry.Warn("App registration failed! App not found!")
				c.AbortWithStatus(http.StatusForbidden)
				return
			}
		} else {
			if err := CompareHashAndPassword(app.Config.Token, token); err == nil {
				logEntry.Debug("App registration attempt (refresh)!")
				c.Status(204)
			} else {
				logEntry.WithError(err).Warn("App registration failed! Wrong token!")
				c.AbortWithStatus(http.StatusForbidden)
				return
			}
		}

		requestId := app.newRequestId()
		addPollRequestLinkHeader(c.Writer.Header(), r.config.GatewayBaseURL, app.Config.Name, requestId, "first")
		addLink(c.Writer.Header(), app.PublicURL, "related")
		c.Writer.Header().Add("X-Rhttp-Name", name)
		logEntry.Debug("App registration successful!")
	}
}

// helper functions

func addLink(header http.Header, url, rel string) {
	header.Add("Link", "<"+url+">; rel=\""+rel+"\"")
}

func addPollRequestLinkHeader(header http.Header, gatewayBaseURL string, appName string, requestId, rel string) {
	addLink(header, gatewayBaseURL+"/r/"+appName+"/"+requestId, rel)
}
