/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"crypto/tls"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/braintree/manners"
	"github.com/gin-gonic/gin"
)

const (
	APP_VAR = "APP-NAME"
)

type rhttpGateway struct {
	clientRouter   *gin.Engine
	frontendRouter *gin.Engine
	appRegistry    *appRegistry
	userRegistry   *userRegistry
	config         *ServerConfig
	stopNotifier   *StopNotifier
}

func NewRhttpGateway(config *ServerConfig) *rhttpGateway {
	r := &rhttpGateway{config: config}
	r.init()
	return r
}

func (r *rhttpGateway) init() {

	gin.SetMode(gin.ReleaseMode)
	//if Mode() == DebugMode {
	//	gin.SetMode(gin.DebugMode)
	//}

	r.stopNotifier = NewStopNotifier()

	r.userRegistry = NewUserRegistry()
	r.userRegistry.load(r.config)

	r.appRegistry = NewAppRegistry()
	r.appRegistry.load(r.config, r.userRegistry)

	//client router
	{
		r.clientRouter = gin.New()
		r.clientRouter.Use(RouterLogger("CLIENT"))
		r.clientRouter.Use(httpErrorHandleFunc)
		{
			u, err := url.Parse(r.config.GatewayBaseURL)
			if err != nil {
				log.WithError(err).Fatal("wrong url")
				return
			}
			rhttpClientGroup := r.clientRouter.Group(u.Path)
			rhttpClientGroup.POST("", registerClientHandler(r))
			rhttpClientGroup.GET("", ShowInfo())
			{
				rhttpClientRequestGroup := rhttpClientGroup.Group("/r")
				rhttpClientRequestGroup.Use(findAppHandler(r))
				rhttpClientRequestGroup.GET("/:app/:id", pollHandler(r))
				rhttpClientRequestGroup.POST("/:app/:id", responseHandler(r))
			}

			// admin handler
			{
				adminGroup := rhttpClientGroup.Group("/admin", noAdminPassword(r), AdminBasicAuth(r))
				addAdminHandlers(r, adminGroup)
			}
		}

		r.clientRouter.Use(Recovery())
	}

	//frontend router
	r.frontendRouter = gin.New()
	r.frontendRouter.Use(RouterLogger("FRONTEND"))
	r.frontendRouter.Use(httpErrorHandleFunc)
	r.frontendRouter.Use(AppToContextByHostname(r))
	r.frontendRouter.Use(FrontendBasicAuth())
	r.frontendRouter.Any("/*all", frontendHandler(r))
	r.frontendRouter.Use(Recovery())

}

func (r *rhttpGateway) Run() {
	var wg sync.WaitGroup

	clientServer := manners.NewWithServer(
		&http.Server{Addr: r.config.ClientBinding, Handler: r.clientRouter})
	frontendServer := manners.NewWithServer(
		&http.Server{Addr: r.config.FrontendBinding, Handler: r.frontendRouter})

	shutdown := func() {
		log.Info("Shutting down...")
		clientServer.Close()
		frontendServer.Close()
		r.stopNotifier.NotifyListeners()
	}

	starter := func(server *manners.GracefulServer, name string, addr string, tlsc []TlsCertConfig) {
		wg.Add(1)
		go func() {
			defer wg.Done()

			ln, err := Listener(addr, tlsc, false)
			if err != nil {
				log.WithError(err).WithField("server", name).
					Fatal("Fatal error: Program termination!")
				return
			}

			if err := server.Serve(ln); err != nil {
				log.WithError(err).WithField("server", name).
					Fatal("Fatal error: Program termination!")
				return
			}
			log.Info(name, " stopped.")
		}()
	}

	starter(clientServer, "app client server", r.config.ClientBinding, r.config.ClientTls)
	starter(frontendServer, "frontend server", r.config.FrontendBinding, r.config.FrontendTls)

	go func() {
		sigchan := make(chan os.Signal, 1)
		signal.Notify(sigchan, os.Interrupt, os.Kill)
		<-sigchan
		shutdown()
	}()

	wg.Wait()
	log.Info("Shutdown completed.")
}

// tcpKeepAliveListener sets TCP keep-alive timeouts on accepted
// connections.
// Copy from net/http/server.go
// Copyright: The Go Authors
type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	tc.SetKeepAlive(true)
	tc.SetKeepAlivePeriod(3 * time.Minute)
	return tc, nil
}

///////////////////////////////////////

func Listener(addr string, tlsc []TlsCertConfig, disableTLS bool) (net.Listener, error) {

	// tcp listener
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	// wrap with tcp keep alive listener
	ln = tcpKeepAliveListener{ln.(*net.TCPListener)}

	if len(tlsc) == 0 || disableTLS {
		// seems there is no TLS configured
		return ln, nil
	}

	// TLS configuration
	config := &tls.Config{}
	config.NextProtos = []string{"http/1.1"}
	config.Certificates = make([]tls.Certificate, len(tlsc))
	for i, c := range tlsc {
		config.Certificates[i], err = tls.LoadX509KeyPair(c.CertFile, c.KeyFile)
		if err != nil {
			return nil, err
		}
	}

	// Build mapping name->cert for SNI
	config.BuildNameToCertificate()

	return tls.NewListener(ln, config), nil
}
