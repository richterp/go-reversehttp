/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import "sync"

type appRegistry struct {
	registry           map[string]*app
	registryM          sync.RWMutex
	RegisterNewAppLock sync.Mutex
	wildcardAppCount   int
	wildcardAppFmt     string
}

func NewAppRegistry() *appRegistry {
	return &appRegistry{registry: make(map[string]*app)}
}

func (r *appRegistry) load(sc *ServerConfig, ur *userRegistry) error {
	r.registryM.Lock()
	defer r.registryM.Unlock()

	r.wildcardAppCount = sc.WildcardApps.Count
	r.wildcardAppFmt = sc.WildcardApps.Fmt

	for _, ac := range sc.Apps {
		ac := ac

		app, err := newApp(&ac, sc, ur, true)
		if err != nil {
			return err
		}

		r.addWithLock(app, false)
	}
	return nil
}

func (ar *appRegistry) findApp(appId string) (*app, bool) {
	ar.registryM.RLock()
	app, ok := ar.registry[appId]
	ar.registryM.RUnlock()
	return app, ok
}

func (ar *appRegistry) delete(app *app) {
	ar.registryM.Lock()
	delete(ar.registry, app.Config.Name)
	ar.registryM.Unlock()
}

func (ar *appRegistry) add(app *app) *app {
	return ar.addWithLock(app, true)
}

func (ar *appRegistry) addWithLock(app *app, lock bool) *app {
	if lock {
		ar.registryM.Lock()
		defer ar.registryM.Unlock()
	}

	ar.registry[app.Config.Name] = app
	app.leaseTimer = CreateAndStartLeaseTimer(app.Config.Lease, func() {
		ar.delete(app)
	})

	return app
}
