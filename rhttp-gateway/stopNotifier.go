/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"sync"

	log "github.com/Sirupsen/logrus"
)

type stopNotifierKey *byte

type StopNotifier struct {
	listener map[stopNotifierKey]chan struct{}
	mutex    sync.RWMutex
}

func NewStopNotifier() *StopNotifier {
	return &StopNotifier{listener: make(map[stopNotifierKey]chan struct{})}
}

func (stn *StopNotifier) StopNotify() (stopNotifierKey, <-chan struct{}) {
	stn.mutex.Lock()
	defer stn.mutex.Unlock()

	c := make(chan struct{}, 1)

	if stn.listener == nil {
		c <- struct{}{}
		return nil, c
	}
	id := uint8(1)
	stn.listener[&id] = c
	return &id, c
}

func (stn *StopNotifier) NotifyListeners() {
	stn.mutex.Lock()
	defer stn.mutex.Unlock()
	log.Debug("stop notify count: ", len(stn.listener))
	for _, listener := range stn.listener {
		listener <- struct{}{}
	}
	stn.listener = nil
}

func (stn *StopNotifier) Delete(id stopNotifierKey) {
	if id == nil {
		return
	}
	stn.mutex.Lock()
	defer stn.mutex.Unlock()
	delete(stn.listener, id)
}
