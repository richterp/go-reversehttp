/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"sync/atomic"
	"time"
)

type LeaseTimer struct {
	leaseTimer    *time.Timer
	activeClients int32
	lease         int
}

func CreateAndStartLeaseTimer(lease int, f func()) *LeaseTimer {
	result := &LeaseTimer{lease: lease}
	if lease >= 0 {
		result.leaseTimer = time.AfterFunc(time.Duration(lease)*time.Second, f)
	}
	return result
}

func (app *LeaseTimer) StopLeaseTimer() {
	atomic.AddInt32(&app.activeClients, 1)
	t := app.leaseTimer
	if t != nil && !t.Stop() {
		select {
		case <-t.C:
		default:
		}
	}
}

func (app *LeaseTimer) ResetLeaseTimer() {
	i := atomic.AddInt32(&app.activeClients, -1)
	t := app.leaseTimer
	if t != nil {
		if !t.Stop() {
			select {
			case <-t.C:
			default:
			}
		}
		if i == 0 {
			app.leaseTimer.Reset(time.Duration(app.lease) * time.Second)
		}
	}
}
