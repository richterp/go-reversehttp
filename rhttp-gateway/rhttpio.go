/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"bufio"

	"github.com/gin-gonic/gin"

	"io"
	"io/ioutil"
	"net/http"
)

func discard(src io.ReadCloser) {
	io.Copy(ioutil.Discard, src)
}

// write frontend request to poll response
func writeRequestToResponse(request *http.Request, responseWriter http.ResponseWriter) error {
	if err := request.WriteProxy(responseWriter); err != nil {
		discard(request.Body)
		return err
	}
	return nil
}

// write response entity to frontend response
func writeResponseEntityToResponse(request *http.Request, rw http.ResponseWriter) error {
	resp, err := http.ReadResponse(bufio.NewReader(request.Body), nil)
	if err != nil {
		return err
	}

	copyHeader(rw.Header(), resp.Header)
	rw.WriteHeader(resp.StatusCode)

	// Flush headers
	if flusher, ok := rw.(http.Flusher); ok {
		if ginRw, ok := rw.(gin.ResponseWriter); ok {
			//TODO: Open an issue at "gin" project: Flush() should call WriteHeaderNow() at first.
			ginRw.WriteHeaderNow()
			flusher.Flush()
		}
	}

	defer resp.Body.Close()

	var bufferSize uint64
	if resp.ContentLength < 0 {
		// length is unknown.
		bufferSize = 32 * 1024
	} else {
		bufferSize = uint64_min(256*1024, uint64(resp.ContentLength))
	}
	if bufferSize > 0 {
		buffer := make([]byte, bufferSize)
		if _, err := io.CopyBuffer(rw, resp.Body, buffer); err != nil {
			return err
		}
	}

	return nil
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}
