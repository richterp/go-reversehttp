/* Copyright (C) 2017 Pascal Richter (pascal.richter@kupanet.de)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"fmt"
	"net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
)

var CreateHandlerLog func(name string, c *gin.Context, fields log.Fields) *log.Entry

func init() {
	CreateHandlerLog = func(name string, c *gin.Context, fields log.Fields) *log.Entry {
		return log.WithField("_name", name).WithFields(fields)
	}
}

func ShowInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.String(http.StatusOK, "%s v%s / %s \n%s", APP_NAME, APP_VERSION, APP_USAGE, APP_COPYRIGHT)
	}
}

func Recovery() gin.HandlerFunc {
	return gin.RecoveryWithWriter(log.StandardLogger().Out)
}

func noAdminPassword(r *rhttpGateway) gin.HandlerFunc {
	return func(c *gin.Context) {
		if len(r.config.AdminPassword) == 0 {
			c.Writer.WriteString("no admin password set.\n")
			c.AbortWithStatus(500)
			return
		}
		c.Next()
	}
}

func httpErrorHandleFunc(c *gin.Context) {
	c.Next()
	if status := c.Writer.Status(); status >= 400 {
		c.Writer.WriteString(fmt.Sprintf("%d - %s", status, http.StatusText(status)))
		c.Abort()
	}
}

func RouterLogger(routerName string) gin.HandlerFunc {
	return func(c *gin.Context) {

		if log.GetLevel() >= log.DebugLevel {
			CreateHandlerLog(routerName, c, log.Fields{
				"method": c.Request.Method,
				"host":   c.Request.Host,
				"path":   c.Request.RequestURI,
			}).Debug("Request received.")
		}

		c.Next()

		if c.Writer.Status() >= 500 || log.GetLevel() >= log.DebugLevel {
			entry := CreateHandlerLog(routerName, c, log.Fields{
				"method": c.Request.Method,
				"host":   c.Request.Host,
				"path":   c.Request.RequestURI,
				"status": c.Writer.Status(),
				"size":   c.Writer.Size(),
			})

			msg := "Request handled."

			if c.Writer.Status() >= 500 {
				entry.Error(msg)
			} else {
				entry.Debug(msg)
			}
		}
	}
}
