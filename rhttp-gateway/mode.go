package main

const (
	ProdMode  string = "prod"
	DebugMode string = "debug"
)

var mode string = ProdMode

func SetMode(value string) {
	mode = value
}

func Mode() string {
	return mode
}
