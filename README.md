# Reverse HTTP

## Intro

This project aims to implement a _Reverse HTTP Gateway_ and a _Reverse HTTP Application Adapter_ for the [Reverse HTTP](http://reversehttp.net/reverse-http-spec.html) protocol.

Reverse HTTP offers a solution to provide access from the internet to HTTP-based web services and applications, which are located in secured networks. This even works if the device which is running the web service or application is behind a NAT router, firewall or proxy server. The device becomes just another host on the internet, addressable via its own URL.
On the web service providing side, Reverse HTTP works WITHOUT changes to the configuration of the NAT router or the firewall. This is different from providing a web service via a reverse http proxy.

## Components

The participants in the Reverse HTTP protocol are Applications and the Gateway Service.

A Reverse HTTP Application is a program that wishes to provide a HTTP service to third parties, and that registers with a Reverse HTTP Gateway Service in order to do so. A Reverse HTTP Application can be any HTTP-based web service (e.g. SOAP or REST service) or an end-user frontend which is connected over the _Reverse HTTP Application Adapter_.

A _Reverse HTTP Gateway_ is an ordinary HTTP server that provides Gateway Service to Applications. A Gateway Service

* responds to Application requests for claiming or relinquishing pieces of the URL-space under the control of the Gateway Server, and
* relays incoming requests for claimed pieces of URL-space on to the corresponding Applications, and relays Application responses to such requests on to the original requestors.

This project proides a free implementation of a _Reverse HTTP Gateway_ in the go programming language.

This project also develops a _Reverse HTTP Application Adapter_ implemented in java. This will be available soon.

# Reverse HTTP Gateway

## Build

If you have a working Go installation, you can build the rhttp-gateway by

	go get bitbucket.org/richterp/go-reversehttp/rhttp-gateway

## Install

Build the rhttp-gateway and place the binary somewhere on your PATH.  
Alternatively you can [download binaries](https://bitbucket.org/richterp/go-reversehttp/downloads) for your OS.

## Quick start example

For the example, make sure that the hostname foo.localhost is resolved to your loopback address. Therefore you can place an entry in your `/etc/hosts`:

	127.0.0.1 		foo.localhost

Place a gateway configuration file `rhttp-gateway.xml` somewhere in your file system containing:

	<?xml version="1.0" encoding="UTF-8"?>
	<gateway>

		<!-- Frontend template url. "APP-NAME" is used as variable for concrete app names. -->
		<frontendURL>http://APP-NAME.localhost:8001</frontendURL>

		<!-- RHTTP client registration url -->
		<clientURL>http://localhost:8000/reversehttp</clientURL>

		<!-- Act as an open gateway -->
		<openGateway>true</openGateway>

	</gateway>

Start rhttp-gateway with this configuration:

	rhttp-gateway --config "/path/to/rhttp-gateway.xml"

Reverse HTTP Application `foo` can now provide its service at  `http://foo.localhost:8001` by registering and claiming this URL-space at the gateway service URL at `http://localhost:8000/reversehttp`.

# Copyright and License

## Reverse HTTP Gateway (Software)
Copyright (C) 2017 Pascal Richter <pascal.richter@kupanet.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## Reverse HTTP Protocol Specification

http://reversehttp.net/reverse-http-spec.html

Copyright © 2009, 2010 Tony Garnock-Jones <tonygarnockjones@gmail.com>  
Copyright © 2009 LShift Ltd. <query@lshift.net>

Permission is hereby granted, free of charge, to any person obtaining a copy of this documentation (the “Documentation”), to deal in the Documentation without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Documentation, and to permit persons to whom the Documentation is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Documentation.

THE DOCUMENTATION IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE DOCUMENTATION OR THE USE OR OTHER DEALINGS IN THE DOCUMENTATION.
